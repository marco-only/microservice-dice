package at.mavila.dice;

import java.math.BigDecimal;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.math.Fraction;
import org.apache.commons.math3.fraction.FractionFormat;

public final class Rational {

  private static final ThreadLocal<FractionFormat> FRACTION_FORMAT_THREAD_LOCAL = ThreadLocal.withInitial(
    FractionFormat::getProperInstance
  );

  private Rational() {
    // Forbidden to create instances of this class.
  }

  public static String getFraction(final BigDecimal bigDecimal) {
    Validate.notNull(bigDecimal, "Parameter decimal must not be null");

    if (bigDecimal.stripTrailingZeros().equals(BigDecimal.ONE)) {
      return "1/1";
    }

    return FRACTION_FORMAT_THREAD_LOCAL
      .get()
      .format(Fraction.getFraction(bigDecimal.doubleValue()))
      .replaceAll("\\s+", "");
  }
}
