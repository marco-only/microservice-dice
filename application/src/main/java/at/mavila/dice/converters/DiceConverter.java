package at.mavila.dice.converters;

import java.util.Date;

import org.apache.commons.lang3.Validate;
import org.javatuples.Pair;
import org.springframework.stereotype.Service;

import at.mavila.dice.db.entities.Dice;
import at.mavila.dice.web.model.ResultDiceProbability;

@Service
public class DiceConverter {

    public Pair<Dice, ResultDiceProbability> fromInnerToApi(final Dice dice) {

        Validate.notNull(dice, "Dice must not be null");

        final ResultDiceProbability resultDiceProbability = new ResultDiceProbability();
        resultDiceProbability.setDecimal(dice.getResult_decimal());
        resultDiceProbability.setFraction(dice.getResult_fraction());
        resultDiceProbability.setMessage("Cached at: " + new Date());

        return Pair.with(dice, resultDiceProbability);

    }

    public Pair<Dice, ResultDiceProbability> fromApiToInner(final ResultDiceProbability resultDiceProbability) {

        Validate.notNull(resultDiceProbability, "ResultDiceProbability must not be null");

        final Dice dice = new Dice();
        dice.setResult_decimal(resultDiceProbability.getDecimal());
        dice.setResult_fraction(resultDiceProbability.getFraction());

        return Pair.with(dice, resultDiceProbability);
    }

}