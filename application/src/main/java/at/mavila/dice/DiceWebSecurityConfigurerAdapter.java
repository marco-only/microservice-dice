package at.mavila.dice;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.server.resource.authentication.JwtIssuerAuthenticationManagerResolver;
import at.mavila.dice.services.JwtIssuerService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class DiceWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(DiceWebSecurityConfigurerAdapter.class);

    private final JwtIssuerService jwtIssuerService;

    @Autowired
    public DiceWebSecurityConfigurerAdapter(final JwtIssuerService jwtIssuerService) {
        this.jwtIssuerService = jwtIssuerService;
    }

    @Override
    protected void configure(HttpSecurity http) {
        try {
            http.csrf().disable();
            http.authorizeRequests().antMatchers("/actuator/prometheus").permitAll();
            setSecure(http);
            http.cors();
            LOGGER.info("Configured security.");
        } catch (Exception throwable) {
            throw new RuntimeException(throwable.getMessage(), throwable);
        }
    }

    private void setSecure(HttpSecurity http) throws Exception {
        http.oauth2ResourceServer(oAuth2ResourceServerSpec -> {
            final JwtIssuerAuthenticationManagerResolver managerResolver =
                    this.jwtIssuerService.getJwtIssuerAuthenticationManagerResolver();
            LOGGER.info("JWT Issuer: {}", managerResolver);
            oAuth2ResourceServerSpec.authenticationManagerResolver(managerResolver);
        }).authorizeRequests().anyRequest().authenticated();
    }
}