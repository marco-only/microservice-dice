package at.mavila.dice.db.entities;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Stores properties for the Tenant and client id.
 */
public class TenantAuthorizationProvider implements Serializable {

    private static final long serialVersionUID = -1349558362348180233L;
    private final String url;
    private final String id;

    public TenantAuthorizationProvider(final String url, final String id) {
        this.url = url;
        this.id = id;
    }

    public String getUrl() {
        return this.url;
    }

    public String getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("url", this.url)
                .append("id", this.id)
                .toString();
    }
}