package at.mavila.dice.db.access;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import at.mavila.dice.db.entities.Dice;

@Repository
public interface DiceRepository extends JpaRepository<Dice, Long> {

    /**
     * Gets all Dice from database
     */
    List<Dice> findAll();

    /**
     * Tried to cache here, no luck.
     */
    <S extends Dice> S save(S entity);

    // @CachePut(cacheNames = "dice_cache", key = "{#entity.number, #entity.positive_cases}")
    // <S extends Dice> S save(S entity);

    /**
     * Gets the stored calculation given the number, positive_cases.
     * 
     * @param number         (it is the number of dice)
     * @param positive_cases (parameter coming from the user indicating )
     * @return a Dice object.
     */
    @Query(value = " from Dice d where d.number = :number and d.positive_cases = :positive_cases", nativeQuery = false)
    Dice findByNumberAndPositiveCasesAndResult(
            @Param("number") final long number,
            @Param("positive_cases") final long positive_cases);

}
