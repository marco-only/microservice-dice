package at.mavila.dice.db.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 * Database storage
 */
@Entity
@Table(name = "DICE", uniqueConstraints = {
        @UniqueConstraint(name = "DiceAndPositiveCases", columnNames = { "number", "positive_cases" })
})
public class Dice extends BaseEntity {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 78473473434L;

    @Column(name = "number", nullable = false)
    @NotNull
    private long number;

    @Column(name = "positive_cases", nullable = false)
    @NotNull
    private long positive_cases;

    @Column(name = "result_fraction", nullable = false)
    @NotNull
    private String result_fraction;

    @Column(name = "result_decimal", nullable = false)
    @NotNull
    private double result_decimal;

    public Dice() {
        // Empty constructor
    }

    public Dice(
            final long number,
            final long positive_cases) {
        this.number = number;
        this.positive_cases = positive_cases;
    }

    public Dice(
            final long number,
            final long positive_cases,
            final String result_fraction,
            final double result_decimal) {

        this.number = number;
        this.positive_cases = positive_cases;
        this.result_fraction = result_fraction;
        this.result_decimal = result_decimal;

    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public long getPositive_cases() {
        return positive_cases;
    }

    public void setPositive_cases(long positive_cases) {
        this.positive_cases = positive_cases;
    }

    public String getResult_fraction() {
        return result_fraction;
    }

    public void setResult_fraction(String result_fraction) {
        this.result_fraction = result_fraction;
    }

    public double getResult_decimal() {
        return result_decimal;
    }

    public void setResult_decimal(double result_decimal) {
        this.result_decimal = result_decimal;
    }

    @Override
    public String toString() {
        return "Dice [number=" + number + ", positive_cases=" + positive_cases + ", result_decimal=" + result_decimal
                + ", result_fraction=" + result_fraction + "]";
    }

    

}
