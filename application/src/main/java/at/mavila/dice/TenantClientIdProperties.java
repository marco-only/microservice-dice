package at.mavila.dice;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import at.mavila.dice.db.entities.TenantAuthorizationProvider;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
@ConfigurationProperties(prefix = "app")
public class TenantClientIdProperties {

    private static final ThreadLocal<Pattern> PATTERN_THREAD_LOCAL = ThreadLocal.withInitial(() -> Pattern.compile("^(https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*))$"));

    private Map<String, String> tenants;

    private String current_tenant;

    public List<TenantAuthorizationProvider> getTenants() {
        return this.tenants.values().stream().map(this::findUrl).collect(Collectors.toList());
    }

    public void setTenants(Map<String, String> tenants) {
        this.tenants = tenants;
    }

    private TenantAuthorizationProvider findUrl(String value) {
        var regexMatcher = PATTERN_THREAD_LOCAL.get().matcher(value);

        if (regexMatcher.find()) {
            return new TenantAuthorizationProvider(
                    regexMatcher.group(1).trim(),
                    StringUtils.EMPTY);
        }

        throw new NullPointerException("Property not correctly set: " + value);
    }

    public String getCurrent_tenant() {
        return this.current_tenant;
    }

    public void setCurrent_tenant(String current_tanant) {
        this.current_tenant = current_tanant;
    }

    @PreDestroy
    public void destroy() {
        PATTERN_THREAD_LOCAL.remove();
    }
}