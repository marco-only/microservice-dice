package at.mavila.dice.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import at.mavila.dice.db.entities.Dice;
import at.mavila.dice.services.DiceService;
import at.mavila.dice.web.api.DiceProbabilityApi;
import at.mavila.dice.web.model.ResultDiceProbability;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@Controller
@RequestMapping("api")
@Slf4j
public class DiceController implements DiceProbabilityApi {

    private final DiceService diceService;

    @Autowired
    public DiceController(final DiceService diceService) {
        this.diceService = diceService;
    }

    /**
     * Calculates the probability to throw dices having the positive cases known
     * beforehand
     * 
     * <pre>
     *  - What is the probability to throw a dice and get 1,2 or 3: curl -s -w '\n' -X GET 'http://localhost:8080/api/dice_probability?dices=1&positive_cases=3' Response: {"fraction":"1/2","decimal":0.5,"message":"OK: Mon Dec 13 13:26:01 UTC 2021"}
     *  - What is the probability to throw two dices and get the 1:1 combination:  curl -s -w '\n' -X GET 'http://localhost:8080/api/dice_probability?dices=2&positive_cases=1' Response: {"fraction":"1/36","decimal":0.027777777778,"message":"OK: Mon Dec 13 13:25:20 UTC 2021"}
     * </pre>
     * 
     * @param Long dices
     * @param Long positiveCases
     * @return a ResponseEntity as specified
     */
    @Override
    public ResponseEntity<ResultDiceProbability> diceProbabilty(
            final Long diceP, final Long positiveCases) {

        StopWatch stopWatch = new StopWatch("diceProbabilty");
        stopWatch.start();

        if (log.isDebugEnabled()) {
            log.debug("Dices: {}", diceP);
            log.debug("Positive cases: {}", positiveCases);
        }

        final Optional<ResultDiceProbability> result = this.diceService.get(new Dice(diceP, positiveCases));

        stopWatch.stop();
        log.info("Stopwatch (controller): {} milliseconds", stopWatch.getLastTaskInfo().getTimeMillis());
        log.info("Stopwatch (controller): {} nanoseconds", stopWatch.getLastTaskInfo().getTimeNanos());

        return ResponseEntity.ok(result.get());

    }

}