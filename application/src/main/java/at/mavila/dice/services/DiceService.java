package at.mavila.dice.services;

import static java.math.BigDecimal.valueOf;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;

import javax.transaction.Transactional;

import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
//import simple producer packages
import org.apache.kafka.clients.producer.Producer;

//import KafkaProducer packages
import org.apache.kafka.clients.producer.KafkaProducer;

//import ProducerRecord packages
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import at.mavila.dice.Rational;
import at.mavila.dice.converters.DiceConverter;
import at.mavila.dice.db.access.DiceRepository;
import at.mavila.dice.db.entities.Dice;
import at.mavila.dice.web.model.ResultDiceProbability;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class DiceService {

    private static final long DICE_FACES = 6;

    private final DiceRepository diceRepository;
    private final DiceConverter diceConverter;

    @Autowired
    public DiceService(final DiceRepository diceRepository, final DiceConverter diceConverter) {
        this.diceRepository = diceRepository;
        this.diceConverter = diceConverter;

    }

    @Cacheable(value = "dice_cache")
    public Optional<ResultDiceProbability> get(final Dice dice) {

        StopWatch stopWatch = new StopWatch("diceProbabilty");
        stopWatch.start();

        final Optional<Dice> result = Optional
                .ofNullable(this.diceRepository.findByNumberAndPositiveCasesAndResult(dice.getNumber(),
                        dice.getPositive_cases()));

        if (result.isEmpty()) {
            Pair<Optional<Dice>, Optional<ResultDiceProbability>> probability = this
                    .calculateProbability(dice.getNumber(), dice.getPositive_cases());

            if (probability.getValue0().isEmpty()) {
                return probability.getValue1();
            }

            Dice dicePrepared = this.prepareDice(dice, probability);

            stopWatch.stop();
            log.info("Stopwatch (saved): {} milliseconds", stopWatch.getLastTaskInfo().getTimeMillis());

            return this.save(dicePrepared);
        }

        final Dice resultDice = result.get();
        final Pair<Dice, ResultDiceProbability> pair = this.diceConverter.fromInnerToApi(resultDice);

        stopWatch.stop();
        log.info("Stopwatch (cache): {} milliseconds", stopWatch.getLastTaskInfo().getTimeMillis());

        return Optional.of(pair.getValue1());
    }

    private Dice prepareDice(final Dice dice, Pair<Optional<Dice>, Optional<ResultDiceProbability>> probability) {
        final Dice rawDice = probability.getValue0().get();
        rawDice.setNumber(dice.getNumber());
        rawDice.setPositive_cases(dice.getPositive_cases());
        return rawDice;
    }

    private Pair<Optional<Dice>, Optional<ResultDiceProbability>> calculateProbability(
            final Long diceP,
            final Long positiveCases) {
        final long possibleCases = this.possibleCases(diceP);

        if (positiveCases > possibleCases) {

            ResultDiceProbability resultDiceProbability = new ResultDiceProbability();
            resultDiceProbability.setMessage(String.format(
                    "Positive cases cannot be bigger than possible cases. Possible cases: <%d> Positive cases: <%d>",
                    possibleCases, positiveCases));

            return Pair.with(Optional.empty(), Optional.of(resultDiceProbability));
        }

        BigDecimal bdPositiveCases = valueOf(positiveCases);
        BigDecimal bdPossibleCases = valueOf(possibleCases);

        BigDecimal divide = bdPositiveCases.divide(bdPossibleCases, 12, RoundingMode.HALF_UP);

        ResultDiceProbability result = new ResultDiceProbability();

        final String fraction = Rational.getFraction(divide);
        result.setFraction(fraction);

        final double decimal_result = divide.doubleValue();
        result.setDecimal(decimal_result);
        result.setMessage("OK: " + new Date());

        final Dice dice = new Dice();
        dice.setResult_decimal(decimal_result);
        dice.setResult_fraction(fraction);

        return Pair.with(Optional.of(dice), Optional.of(result));
    }

    private long possibleCases(long dices) {
        long result = 1;
        while (dices > 0) {

            result = DICE_FACES * result;

            --dices;
        }
        return result;
    }

    /**
     * Save a dice in the database.
     * 
     * @param dice
     * @return an Optional. Empty when the parameter is null. Empty when didn't save
     *         in the database.
     */
    @CachePut(value = "dice_cache")
    private Optional<ResultDiceProbability> save(final Dice dice) {
        Optional<Dice> optionalDice = Optional.ofNullable(dice);

        if (optionalDice.isEmpty()) {
            return Optional.empty();
        }

        Optional<Dice> result = Optional.ofNullable(diceRepository.save(dice));

        if (result.isEmpty()) {
            return Optional.empty();
        }

        final Pair<Dice, ResultDiceProbability> pair = this.diceConverter.fromInnerToApi(result.get());

        Producer<String, String> producer = null;
        try {
            final Properties properties = new Properties();
            properties.put("bootstrap.servers", "kafka0:29092");
            properties.put("acks", "all");
            properties.put("retries", "0");
            properties.put("batch.size", 16384);
            properties.put("linger.ms", 1);
            properties.put("buffer.memory", 33554432);

            properties.put("key.serializer",
                    "org.apache.kafka.common.serialization.StringSerializer");

            properties.put("value.serializer",
                    "org.apache.kafka.common.serialization.StringSerializer");

            producer = new KafkaProducer<>(properties);

            ProducerRecord<String, String> producerRecord = new ProducerRecord<>("quickstart-events",
                    pair.getValue0().toString(), pair.getValue1().toString());

            producer.send(producerRecord);
        } catch (Exception exception) {
            log.warn("Exception caught: <{}>", exception.getMessage());
        } finally {
            if (Objects.nonNull(producer)) {
                producer.flush();
                producer.close();
            }
        }

        return Optional.of(pair.getValue1());
    }

}
