package at.mavila.dice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.server.resource.authentication.JwtIssuerAuthenticationManagerResolver;
import org.springframework.stereotype.Service;
import at.mavila.dice.TenantClientIdProperties;
import at.mavila.dice.db.entities.TenantAuthorizationProvider;

import javax.annotation.PostConstruct;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class JwtIssuerService {

    private final TenantClientIdProperties tenantClientIdProperties;

    private JwtIssuerAuthenticationManagerResolver jwtIssuerAuthenticationManagerResolver;

    @Autowired
    public JwtIssuerService(TenantClientIdProperties tenantClientIdProperties) {
        this.tenantClientIdProperties = tenantClientIdProperties;
    }

    @PostConstruct
    public void init() {
        this.jwtIssuerAuthenticationManagerResolver = new JwtIssuerAuthenticationManagerResolver(getUrls());
    }

    public JwtIssuerAuthenticationManagerResolver getJwtIssuerAuthenticationManagerResolver() {
        return this.jwtIssuerAuthenticationManagerResolver;
    }

    private Set<String> getUrls() {
        return this.tenantClientIdProperties
                .getTenants()
                .stream()
                .map(TenantAuthorizationProvider::getUrl)
                .collect(Collectors.toSet());
    }
}
