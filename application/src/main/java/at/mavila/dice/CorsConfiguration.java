package at.mavila.dice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Added Cors global support as documented in:
 * https://spring.io/blog/2015/06/08/cors-support-in-spring-framework
 */
@Configuration
public class CorsConfiguration {

  @Bean
  public WebMvcConfigurer corsConfigurer() {
    return new WebMvcConfigurerImpl();
  }

  static class WebMvcConfigurerImpl implements WebMvcConfigurer {

    private static final String PATH_PATTERN = "/**";
    private static final Logger LOGGER = LoggerFactory.getLogger(
        WebMvcConfigurerImpl.class);

    public WebMvcConfigurerImpl() {
      LOGGER.info("A new CORS configurer");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
      LOGGER.info("CORS mapping: <{}>", PATH_PATTERN);
      registry.addMapping(PATH_PATTERN);
    }
  }
}
