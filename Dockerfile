FROM openjdk:11
# The application's jar file
ARG JAR_FILE=application/build/libs/application-1.0-SNAPSHOT.jar
# Add the application's jar to the container
COPY ${JAR_FILE} application.jar
# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-XX:+UnlockExperimentalVMOptions","-XX:+UseZGC","-jar","application.jar"]
