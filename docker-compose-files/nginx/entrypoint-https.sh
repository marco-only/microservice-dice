#!/bin/sh
CERT_FILE="/etc/nginx/certs/safespace_test+4.pem"
if [ -f $CERT_FILE ]; then
    echo "$CERT_FILE exist!"
    cp /etc/nginx/nginx.conf.template /etc/nginx/nginx.conf
else
    echo "$CERT_FILE does not exist! Removing SSL config from nginx.conf"
    sed -E '/^\s*(ssl_certificate|listen\s443)/d' /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf
fi
